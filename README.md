# techreview_smartpython

> Date: 20 Novembre 2020
>
> Auteur: Samuel Berrien <samuel.berrien@sciam.fr>
>
> URL de la vidéo: [#2 Le serpent mangera-t-il votre canard ?](https://caitsconsulting.sharepoint.com/:v:/s/SCIAMCommunicationclients/ETgYbydNoG9IuhZec25gtpUBjJkwpNKkJ3elcBM54AuGog?e=1gsVp0)
>

Tech Review : Python can be your best friend, or not !

- argparse
- typing
- code style
- venv

## Virtual Env

 Les environnements virtuels vous permettent de gérer vos dépendances et de déployer plus facilement vos projets en
 production. 

pour procéder sous linux
```bash
$ python -m venv demo  
$ source demo/bin/activate
```                       

sous windows
```cmd 
> python -m venv demo 
> demo\Scripts\activate.bat
```

Créez à la racine du projet un fichier **requirements.txt**  qui indique vos dépendances
```
argparse>=1.4.0
typing>=3.7.4.3
```

Installez vos dépendances grâce au module pip
```bash
$ python -m pip install -r requirements.txt
```       

Consultez les dépendances du virtual env
```bash
$ python -m pip list
```


## Argparse

Afficher votre manuel grâce à l'option `--help`
```bash
$ python arg_adding.py --help
```                         

La librairie argpase permet de construire dynamiquement la sortie de la commande help:
```  
usage: Main - arg_adding.py [-h] -a {3.14159,1.414} pos1 pos2

positional arguments:
  pos1                  First positional argument
  pos2                  Second positional argument

optional arguments:
  -h, --help            show this help message and exit
  -a {3.14159,1.414}, --argument {3.14159,1.414}, --other-name-argument {3.14159,1.414}
                        The argument description (optional)

```   

_Pour aller plus loin: https://docs.python.org/3/library/argparse.html_     

## Typing

## Construire une archive avec setuptools

Pour packager une application un moyen standard est d'utiliser le setuptools. 
Vous pouvez l'installer avec pip. Dans ce projet, le module est installé via le fichier requirement (cf: virtual env).
Vous pouvez cependnat l'installer avec la commande suivante:

```bash
$ python -m pip install --user setuptools wheel
```

Vous devez aussi créer un fichier `setup.py` à la racine de votre projet. Nous en avons placé un dans ce projet pour 
l'exemple mais référez-vous à la documentation officielle pour plus de détails.

Enfin, il vous est possible de créer votre archive avec la commande :

```bash
$ python setup.py bdist_wheel 
```

Cette commande produit un résultat dans le répertoire `dist`.

```bash
$ ls dist/
techreview_smartpython-0.0.1-py3-none-any.whl  techreview_smartpython-0.0.1.tar.gz
```

_Pour aller plus loin: https://packaging.python.org/tutorials/packaging-projects/_

