import argparse
import os
import sys


def main() -> None:
    print(sys.argv)

    main_parser = argparse.ArgumentParser(
        f"Main - {os.path.basename(__file__)}")

    # definir arguments

    args = main_parser.parse_args()

    print(args)


if __name__ == '__main__':
    main()
