import argparse
import os

import sys


def main() -> None:
    args = parse_args(sys.argv[1:])
    print(args)
    print(args.the_argument_destination)
    print(args.pos1)
    print(args.pos2)

    print(sys.argv)


def parse_args(args):
    main_parser = argparse.ArgumentParser(
        f"Main - {os.path.basename(__file__)}")

    main_parser.add_argument(
        "pos1", type=int,
        help="First positional argument")
    main_parser.add_argument(
        "pos2", type=str,
        help="Second positional argument")

    main_parser.add_argument(
        "-a", "--argument", "--other-name-argument",
        type=float,
        required=True,
        dest="the_argument_destination",
        choices=[3.14159, 1.414],
        default=3.14159,
        help="The argument description (optional)")

    return main_parser.parse_args(args)


if __name__ == '__main__':
    main()
