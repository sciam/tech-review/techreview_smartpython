import unittest
import argparse_demo.arg_adding as arg_adding


class TestArgParse(unittest.TestCase):

    def test_parse_args(self):
        argv = ['-a', '3.14159', '3', '4']
        args = arg_adding.parse_args(argv)
        self.assertEqual(3.14159, args.the_argument_destination)
        self.assertEqual(3, args.pos1)
        self.assertNotEqual(4, args.pos2)
        self.assertEqual('4', args.pos2)

    @unittest.expectedFailure
    def test_wrong_args(self):
        argv = ['-a', '55', '3', '4']
        args = arg_adding.parse_args(argv)
        print(args)

    @unittest.expectedFailure
    def test_wrong_type(self):
        argv = ['-a', '3.14159', 'hello', '4']
        args = arg_adding.parse_args(argv)
        print(args)


if __name__ == '__main__':
    unittest.main()
