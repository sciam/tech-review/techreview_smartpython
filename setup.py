import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="techreview_smartpython",  # Replace with your own username
    version="0.0.1",
    author="Samuel Berrien",
    author_email="samuel.berrien@sciam.fr",
    description="Samples of code for coding with python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/sciam/tech-review/techreview_smartpython/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    test_suite="unittest2.collector"
)
