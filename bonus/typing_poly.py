import typing
import abc
import numpy as np

Key = typing.TypeVar("Key")
Res = typing.TypeVar("Res")


class MyMap(typing.Mapping[Key, Res]):
    def __init__(self):
        super(MyMap, self).__init__()
        self.__map: typing.Dict[Key, Res] = {}

    def __len__(self) -> int:
        return len(self.__map)

    def __iter__(self) -> typing.Iterator[Key]:
        return self.__map.keys().__iter__()

    def __getitem__(self, key: Key) -> Res:
        return self.__map[key]

    def __setitem__(self, key: Key, value: Res) -> None:
        self.__map[key] = value

    def sum(self):
        return 42


class MyDict:
    def __init__(self):
        pass

    def __getitem__(self, item):
        return item

    def __len__(self):
        return 2


class MyMapping(typing.Dict[Key, Res]):
    def __init__(self):
        super(MyMapping, self).__init__()
        self.__dict: typing.Dict[Key, Res] = {}

    def __setitem__(self, key: Key, value: Res) -> None:
        self.__dict[key] = value

    def __getitem__(self, k: Key) -> Res:
        return self.__dict[k]

    def __len__(self) -> int:
        return len(self.__dict)

    def __iter__(self) -> typing.Iterator[Key]:
        return self.__dict.keys().__iter__()


class Reducer(typing.Generic[Key, Res]):
    def reduce(self, o: Key) -> Res:
        pass


UserId = typing.NewType('UserId', int)
some_id = UserId(524313)


class Sum(Reducer[str, int]):
    def __init__(self):
        super().__init__()



def print_map(my_dict: typing.Mapping[typing.Any, typing.Any]) -> None:
    for k in my_dict:
        print(my_dict[k])


def print_iterable(iterable) -> None:
    for t in iterable:
        print(t)


if __name__ == '__main__':
    my_list_1: typing.List[int] = list()
    my_list_1.append(1)
    my_list_1.append(2)
    my_list_1.append(3)

    my_map: MyMap = MyMap()
    # my_map["hello"] = 1

    print_iterable(my_map)

    exit()

    print_map({"hello": 1})

    try:
        print_map(my_list_1)
    except IndexError as ie:
        print(ie)

    my_map_2: MyMapping = MyMapping()
    my_map_2["hello"] = 1
    print_map(my_map_2)

    print_iterable(my_map)
    print_iterable(my_map_2)
    print_iterable(my_list_1)
    print_iterable("hello")
    print_iterable(np.ones((10, 2, 3)))

    try:
        print_iterable(4)
    except Exception as e:
        print(e)

    try:
        print_iterable(True)
    except Exception as e:
        print(e)

    try:
        print_iterable(4 + 5j)
    except Exception as e:
        print(e)
