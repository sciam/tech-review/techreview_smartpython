import argparse
from argparse import ArgumentParser, Namespace
from typing import Optional, Text
import os


def main() -> None:
    main_parser = argparse.ArgumentParser(
        f"Main - {os.path.basename(__file__)}")

    demo_subparser = main_parser.add_subparsers(
        title="Demo index",
        required=True,
        dest="demo_index")

    # Group
    opt_group = main_parser.add_argument_group("option")
    opt_group.add_argument("--words", nargs="+", type=str)

    opt_group_2 = argparse.ArgumentParser(add_help=False)
    opt_group_2.add_argument("--words-2", nargs="+", type=str)

    # subparsers
    arg_parser = demo_subparser.add_parser("arg", parents=[opt_group_2])
    action_parser = demo_subparser.add_parser("action", parents=[opt_group_2])

    # Arg
    arg_parser.add_argument(
        "-a", "--argument", "--other-name-argument",
        type=float,
        required=False,
        dest="the_argument_destination",
        nargs="?",
        choices=[3.14159, 1.414],
        default=3.14159,
        help="The argument description (optional)")

    arg_parser.add_argument("pos1", type=int,
                            help="First positional argument")
    arg_parser.add_argument("pos2", type=int,
                            help="Second positional argument")

    # Action
    action_parser.add_argument("--norm", const=False, default=True,
                               action="store_const",
                               dest="bool")
    action_parser.add_argument("--norm-2", action="store_true", dest="bool_2")

    op_group = action_parser.add_mutually_exclusive_group()
    op_group.required = True

    def __mean(l):
        return sum(l) / len(l)

    op_group.add_argument("--mean", action="store_const", const=__mean,
                          dest="op_fun")
    op_group.add_argument("--sum", action="store_const", const=sum,
                          dest="op_fun")

    action_parser.add_argument("floats", default=[], nargs="+", type=float)
    action_parser.add_argument("--float-2", action="append", default=[],
                               type=float)
    action_parser.add_argument("--float-3", nargs="*", default=[], type=float,
                               action="extend")

    action_parser.add_argument("-m", "--mult-2", default=0, action="count")

    class MultPiAction(argparse.Action):

        def __call__(self, parser: ArgumentParser, namespace: Namespace,
                     value: float,
                     option_string: Optional[Text] = ...) -> None:
            if not isinstance(value, float):
                parser.error("Only one value can be mult by PI")
            setattr(namespace, self.dest, value * 3.15159)

    action_parser.add_argument("--single-float", type=float,
                               action=MultPiAction)

    # parse
    args = main_parser.parse_args()
    print(args)


if __name__ == '__main__':
    main()
