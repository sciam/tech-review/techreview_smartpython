import typing

T = typing.TypeVar("T")

Acc = typing.TypeVar("Acc")

Res = typing.TypeVar("Res")


class MyList(typing.List[T]):
    def __init__(self):
        # constructor
        super().__init__()

    def fold(self, first: Acc,
             fn: typing.Callable[[T, Acc], Acc]) -> Acc:
        for t in self:
            first = fn(t, first)
        return first

    def map(self,
            fn: typing.Callable[[T], Res]) -> typing.List[Res]:
        return [fn(t) for t in self]

    def reduce(self,
               fn: typing.Callable[[T, T], T]) -> typing.Optional[T]:
        if len(self) == 0:
            return None
        res = self[0]

        for t in self[1:]:
            res = fn(res, t)

        return res


if __name__ == '__main__':
    l_1: MyList[int] = MyList()
    l_2: MyList[float] = MyList()

    for i in range(10):
        l_1.append(i)
        l_2.append(float(i))
    print(l_1)
    print(l_2)


    def __add_1(n1: int, n2: int) -> int:
        return n1 + n2


    def __add_2(n1: float, n2: float) -> float:
        return n1 + n2


    res_1: typing.Optional[int] = l_1.reduce(__add_1)
    res_2: typing.Optional[float] = l_1.reduce(__add_1)

    res_3: typing.Optional[float] = l_2.reduce(__add_2)
    #res_4: int = l_2.reduce(__add_2)

    #res_5 = l_1.reduce(__add_2)
    #res_6: float = l_1.reduce(__add_2)

    def __mult_pi_add(n: int, acc: float) -> float:
        return acc + float(n) * 3.14159

    res_7: float = l_1.fold(0., __mult_pi_add)
    #res_8: int = l_1.fold(0., __mult_pi_add)

    #res_9: float = l_1.fold(0, __add_2)
    res_10: float = l_1.fold(0, __add_1)

    #res_11: float = l_1.fold(0, __add_2)
    res_12: float = l_1.fold(0., __add_2)

    #res_13: float = l_2.fold(0, __mult_pi_add)
    #res_14: float = l_2.fold(0., __mult_pi_add)

    res_15: float = l_2.fold(0., __add_2)
    #res_16: int = l_2.fold(0., __add_2)

    res_17: float = l_2.fold(0., __add_2)
    #res_18: int = l_2.fold(0., __add_1)

    def __to_float(n: int) -> float:
        return float(n)

    def __to_int(n: float) -> int:
        return int(n)

    res_19: typing.List[float] = l_1.map(__to_float)
    #res_20: typing.List[int] = l_1.map(__to_float)

    res_21: typing.List[int] = l_1.map(__to_int)
    res_22: typing.List[int] = l_2.map(__to_int)
    res_23: typing.List[float] = l_2.map(__to_int)
    res_24: typing.List[float] = l_1.map(__to_float)
