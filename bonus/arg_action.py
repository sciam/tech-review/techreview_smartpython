import argparse
from argparse import ArgumentParser, Namespace
from typing import Optional, Text
import os


def main() -> None:
    action_parser = argparse.ArgumentParser(
        f"Main - {os.path.basename(__file__)}")

    action_parser.add_argument("--norm", const=False, default=True,
                               action="store_const",
                               dest="bool")
    action_parser.add_argument("--norm-2", action="store_true", dest="bool_2")

    op_group = action_parser.add_mutually_exclusive_group()
    op_group.required = True

    def __mean(l):
        return sum(l) / len(l)

    op_group.add_argument("--mean", action="store_const", const=__mean,
                          dest="op_fun")
    op_group.add_argument("--sum", action="store_const", const=sum,
                          dest="op_fun")

    action_parser.add_argument("floats", default=[], nargs="+", type=float)
    action_parser.add_argument("--float-2", action="append", default=[],
                               type=float)
    action_parser.add_argument("--float-3", nargs="*", default=[], type=float,
                               action="extend")

    action_parser.add_argument("-m", "--mult-2", default=0, action="count")

    class MultPiAction(argparse.Action):

        def __call__(self, parser: ArgumentParser, namespace: Namespace,
                     value: float,
                     option_string: Optional[Text] = ...) -> None:
            if not isinstance(value, float):
                parser.error("Only one value can be mult by PI")
            setattr(namespace, self.dest, value * 3.15159)

    action_parser.add_argument("--single-float", type=float,
                               action=MultPiAction)

    args = action_parser.parse_args()

    print(args)

    print(args.op_fun(args.floats + args.float_2 + args.float_3) *
          pow(2, args.mult_2))


if __name__ == '__main__':
    main()
