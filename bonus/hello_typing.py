import typing

Number = typing.TypeVar("Number", int, float)


def div(n1: Number, n2: Number) -> float:
    return n1 / n2


def div_eucli(n1: Number, n2: Number) -> int:
    return n1 // n2


if __name__ == '__main__':
    a: float = div(10, 5)
    b: int = div(10, 5)

    c: float = div(3.14158, 26)
    d: float = div(26, 32)

    e: float = div(10, 3.14159)

    print(e)
