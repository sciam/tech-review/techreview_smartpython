import typing

T = typing.TypeVar("T")

Acc = typing.TypeVar("Acc")


class MyList(typing.List[T]):
    def __init__(self):
        # constructor
        super().__init__()

    def fold(self, first: Acc,
             fn: typing.Callable[[T, Acc], Acc]) -> Acc:
        for t in self:
            first = fn(t, first)
        return first


if __name__ == '__main__':
    my_list: MyList[str] = MyList()

    my_list.append("hello")
    my_list.append("typing")
    my_list.append("!")
    print(my_list)

    # error in mypy
    my_list.append(3.14159)

    print(my_list)

    def __add_length(curr_word: str, total_size: int) -> int:
        return total_size + len(curr_word)

    # no error in mypy
    res_1: int = my_list.fold(0, __add_length)

    def __add_length_untyped(curr_word, acc):
        return 5.3 + curr_word + acc

    # untyped -> no error in mypy
    res_2 = my_list.fold(3.14159, __add_length_untyped)

    def __add_length_mult_pi(curr_word: str, accumulator: float) -> float:
        return accumulator + len(curr_word) * 3.14159

    # error in mypy
    res_3: int = my_list.fold(0, __add_length_mult_pi)
