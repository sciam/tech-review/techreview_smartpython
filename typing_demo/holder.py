import typing

T = typing.TypeVar("T")


class Holder(typing.Generic[T]):
    def __init__(self, a: T):
        self.a = a

    def set_a(self, new_a: T):
        self.a = new_a

    def get_a(self) -> T:
        return self.a


if __name__ == '__main__':
    h = Holder(None)
    # error in mypy
    h.set_a(26)

    h2 = Holder(25)
    # error in mypy
    h2.set_a(None)

    h3 = Holder(True)
    # error in mypy
    h3.set_a(25)

    h4 = Holder(3.14159)
    # no error in mypy
    h4.set_a(1.141)
