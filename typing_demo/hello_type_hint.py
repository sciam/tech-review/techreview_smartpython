# Aide au typage


# /!\ Avant /!\
def f_1(n1, n2):
    return n1 / n2


def f_2(n1, n2):
    return n1 // n2


# python syntax

def div(n1: float, n2: float) -> float:
    return n1 / n2


def div_eucli(n1: int, n2: int) -> int:
    return n1 // n2


if __name__ == '__main__':
    a = f_1(3.14159, 26)

    print(a)

    # no error in mypy -> untyped
    b = f_2(3.14159, 26)

    print(b)

    a_t: float = div(3.14159, 26)

    # error in mypy -> typed
    b_t: int = div_eucli(52.14159, 26)

    print(a_t)
    print(b_t)

    print(type(b_t))
